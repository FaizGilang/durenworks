@extends('layouts.admin')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Contract <small>Add new Contract</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
            <li>
                <i class="fa fa-table"></i> Contract
            </li>
            <li class="active">
                Add
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-sm-12 col-md-12">
        <form method="POST" action="{{ url('/admin/contract/store') }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group{{ $errors->has('id_quo') ? ' has-error' : '' }}">
                <label for="select">Pilih No. Quotation</label>
                <select name="quo" class="form-control" required="required">
                    <option value="" selected>--- Choose Quotation ---</option>
                    @foreach($quo as $q)
					@if( $q->no === '')
					
					@else	
                    <option value="{{ $q->no }}">{{ $q->no }} - {{ $q->proyek }}</option>
					@endif
                    @endforeach
                </select>
                {!! $errors->first('id_quo', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                <label for="input">Nama Contract</label>
                <input type="text" name="nama" class="form-control" placeholder="Input contract name" value="{{ old('nama') }}">
                {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                <label for="textarea">Deskripsi Contract</label>
                <textarea name="deskripsi" class="form-control" placeholder="Input contract description">{{ old('deskripsi') }}</textarea>
                {!! $errors->first('deskripsi', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                <label for="input">File to upload</label>
                <input type="file" name="file" class="form-control" placeholder="input file">
                {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
            </div>

            <input type="submit" name="save" class="btn btn-success" value="Save">
        </form>
    </div>
</div>

@endsection
