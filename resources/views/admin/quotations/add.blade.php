@extends('layouts.admin')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Quotation <small>Add new Quotation</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
            <li>
                <i class="fa fa-edit"></i> Quotation
            </li>
            <li class="active">
                add
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-sm-12 col-md-12">
        <form method="POST" action="{{ url('/admin/quotation/store') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
			@if ( !isset ($id))
            <div class="form-group{{ $errors->has('konsumen') ? ' has-error' : '' }}">
                <label for="input">Nama konsumen</label>
                <input type="text" name="konsumen" class="form-control" placeholder="Input name" value="{{ old('konsumen') }}">
                {!! $errors->first('konsumen', '<p class="help-block">:message</p>') !!}
            </div>
			<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
				<label for="email">Email</label>
				<input type="text" name="email" class="form-control" placeholder="Masukan Email Konsumen" value="{{ old('email') }}">
				{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
			</div>
			<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
				<label for="phone">Nomor Handphone</label>
				<input type="text" name="phone" class="form-control" placeholder="Nomor Handphone" value="{{ old('phone') }}">
				{!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
			</div><div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
				<label for="alamat">Alamat</label>
				<textarea name="alamat" class="form-control" placeholder="Alamat Konsumen" >{{ old('alamat') }}</textarea>
				{!! $errors->first('alamat', '<p class="help-block">:message</p>') !!}
			</div>
				<div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
				<label for="deskripsi">Deskripsi</label>
				<textarea name="deskripsi" class="form-control" placeholder="Deskripsi Proyek" >{{ old('deskripsi') }}</textarea>
				{!! $errors->first('deskripsi', '<p class="help-block">:message</p>') !!}
			</div>

            <div class="form-group{{ $errors->has('proyek') ? ' has-error' : '' }}">
                <label for="input">Nama Proyek</label>
                <input type="text" name="proyek" class="form-control" placeholder="Input project" value="{{ old('proyek') }}">
                {!! $errors->first('proyek', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('term') ? ' has-error' : '' }}">
                <label for="input">Term</label>
                <textarea type="text" name="term" class="form-control" placeholder="add new term">{{ old('term') }}</textarea>
                {!! $errors->first('term', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('waktu') ? ' has-error' : '' }}">
                <label for="input">Waktu pengerjaan</label>
                <input type="text" name="waktu" class="form-control" placeholder="Input working time" value="{{ old('waktu') }}">
                {!! $errors->first('waktu', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('spesifikasi') ? ' has-error' : '' }}">
                <label for="input">Spesifikasi</label>
                <textarea type="text" name="spesifikasi" class="form-control" placeholder="Input specification">{{ old('spesifikasi') }}</textarea>
                {!! $errors->first('spesifikasi', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('harga') ? ' has-error' : '' }}">
                <label for="input">Harga</label>
                <input type="number" step="1" min="0" name="harga" class="form-control" placeholder="Input price" value="{{ old('harga') }}">
                {!! $errors->first('harga', '<p class="help-block">:message</p>') !!}
            </div>

			@else
                <input type="hidden" name="id" value="{{ $id }}">
				<div class="form-group{{ $errors->has('proyek') ? ' has-error' : '' }}">
                <label for="input">Nama Proyek</label>
                <input type="text" name="proyek" class="form-control" placeholder="Input project" value="{{ old('proyek') }}">
                {!! $errors->first('proyek', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('term') ? ' has-error' : '' }}">
                <label for="input">Term</label>
                <textarea type="text" name="term" class="form-control" placeholder="add new term">{{ old('term') }}</textarea>
                {!! $errors->first('term', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('waktu') ? ' has-error' : '' }}">
                <label for="input">Waktu pengerjaan</label>
                <input type="text" name="waktu" class="form-control" placeholder="Input working time" value="{{ old('waktu') }}">
                {!! $errors->first('waktu', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('spesifikasi') ? ' has-error' : '' }}">
                <label for="input">Spesifikasi</label>
                <textarea type="text" name="spesifikasi" class="form-control" placeholder="Input specification">{{ old('spesifikasi') }}</textarea>
                {!! $errors->first('spesifikasi', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('harga') ? ' has-error' : '' }}">
                <label for="input">Harga</label>
                <input type="number" step="1" min="0" name="harga" class="form-control" placeholder="Input price" value="{{ old('harga') }}">
                {!! $errors->first('harga', '<p class="help-block">:message</p>') !!}
            </div>

			@endif
            <input type="submit" name="save" class="btn btn-success" value="Create">
            <input type="submit" name="save" class="btn btn-default" value="Save">
        </form>
    </div>
</div>
@endsection
