@extends('layouts.admin')
@section('content')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Consument <small>Consument Overview</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
            <li class="active">
                <i class="fa fa-user"></i> Consument
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-sm-12 col-md-12">
					@if(Session::has('alert-success'))
					    <div class="alert alert-success">
				            {{ Session::get('alert-success') }}
				        </div>
					@endif
					
					
					<form style="float:left" class="col-md-8"  method="GET" action="{{ url('/admin/project') }}">
						<div class="form-group col-md-6">
							<input type="text" name="s" class="form-control" placeholder="Search Company">
						</div>
						<div class="form-group">
							<button class="btn btn-success">Search</button>
						</div>
					</form>
					

					<div style="float:right" class="col-md-2 btn-group">
 
						<button type="button" class="btn btn-info">Filter Status</button>
						<button type="button>" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
						</button>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('/admin/project/getindex/'.'all')}}">All</a></li>
						<li><a href="{{ url('/admin/project/getindex/'.'prospecting')}}">Prospecting</a></li>
						<li><a href="{{ url('/admin/project/getindex/'.'contacted')}}">Contacted</a></li>
					  </ul>
					</div>
					<br><br>
					<table class="table table-bordered">
						<tr>
							<th style="text-align:center;">No</th>
							<th style="text-align:center;">Company</th>
							<th style="text-align:center;">Contact Person</th>
							<th style="text-align:center;">Email</th>
							<th style="text-align:center;">Phone Number</th>
							<th style="text-align:center;">Address</th>
							<th style="text-align:center;">Status</th>
							<th style="text-align:center;">Action</th>
						</tr>
						<?php $no=1; ?>
						@foreach($projects->sortBy('status') as $projects)
						<tr>
							<td>{{$no++}}</td>
							<td>{{$projects->company}}</td>
							<td>{{$projects->konsumen}}</td>
							<td>{{$projects->email}}</td>
							<td>{{$projects->hp}}</td>
							<td>{{$projects->alamat}}</td>
							<td>{{$projects->status}}</td>
							<td>
								<a href="{{ url('/admin/project/destroy/'.$projects->id) }}" onclick="return confirm('Are you sure to delete this data');" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span><a>
								
									<a href="{{ url('/admin/project/' .$projects->id . '/edit') }}" class="btn btn-primary btn-xs">Edit</a>
		                        	
									@if( $projects->status === 'Prospecting')
										<a href="{{url('/admin/project/contact/'.$projects->id)}}" class="btn btn-success btn-xs">Contact</a>
									@elseif($projects->status === 'Contacted')
										<a href="{{url('/admin/quotation/add/'.$projects->id)}}" class="btn btn-info btn-xs">Discuss</a>
									@endif
		                        
							</td>
						</tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
