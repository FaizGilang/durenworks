@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h3>Add Consument</h3>
			<div class="panel panel-default">
				<div class="panel-body">
					<form action="{{ url('/admin/project/store') }}" method="post">
					{{csrf_field()}}
						<div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
							<label for="company">Company</label>
							<input type="text" name="company" class="form-control" placeholder="Company" value="{{ old('company') }}">
							{!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
							<label for="nama">Contact Person</label>
							<input type="text" name="nama" class="form-control" placeholder="Contact Person Name" value="{{ old('nama') }}">
							{!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="email">Email</label>
							<input type="text" name="email" class="form-control" placeholder="Email Adrress" value="{{ old('email') }}">
							{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
							<label for="phone">Phone Number</label>
							<input type="text" name="phone" class="form-control" placeholder="Phone Number" value="{{ old('phone') }}">
							{!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
							<label for="alamat">Address</label>
							<textarea name="alamat" class="form-control" placeholder="Company Address" >{{ old('alamat') }}</textarea>
							{!! $errors->first('alamat', '<p class="help-block">:message</p>') !!}
						</div>
						
						<div class="form-group">
							<input type="submit" class="btn btn-primary" value="Save">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
