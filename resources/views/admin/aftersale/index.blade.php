@extends('layouts.admin')
@section('content')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            After Sale <small>After Sale Overview</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
            <li class="active">
                <i class="fa fa-wrench"></i> After Sale
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-sm-12 col-md-12">
       
					@if(Session::has('alert-success'))
					    <div class="alert alert-success">
				            {{ Session::get('alert-success') }}
				        </div>
					@endif
					
					<table class="table table-bordered">
						<tr>
							<th style="text-align:center;">No</th>
							<th style="text-align:center;">Quotation</th>
							<th style="text-align:center;">Start Date</th>
							<th style="text-align:center;">End Date</th>
							<th style="text-align:center;">File</th>
							<th style="text-align:center;">Action</th>
						</tr>
						<?php $no=1; ?>
						@foreach($aftersales as $aftersales)
						<tr>
							<td>{{$no++}}</td>
							<td>{{$aftersales->id_quo}}</td>
							<td>{{$aftersales->startdate}}</td>
							<td>{{$aftersales->enddate}}</td>
							<td><a class="btn btn-info" href="{{ url('/aftersale/' . $aftersales->file) }}" target="_blank">show file</a></td>			
	                        <td><a href="{{ url('/admin/aftersale/edit/' .$aftersales->id) }}" class="btn btn-primary btn-xs">Edit</a>
								<a href="{{ url('/sendmail/') }}" class="btn btn-success btn-xs">Send Mail</a>
							</td>
							
						</tr>
						@endforeach
					</table>

					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
