@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h3>Edit After Sale Contracts</h3>
			<div class="panel panel-default">
				<div class="panel-body">
					<form action="{{ url('/admin/aftersale/update') }}" method="post"  enctype="multipart/form-data">
					{{csrf_field()}}				
				
				
				<div class="form-group{{ $errors->has('enddate') ? ' has-error' : '' }}">
					<label for="input">End Date</label>
					<input type="date" name="enddate" class="form-control" placeholder="Input contract name" value="{{$aftersale->enddate}}">
					{!! $errors->first('enddate', '<p class="help-block">:message</p>') !!}
				</div>
				
				<div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
					<label for="input">Change Contract :</label>
					<a class="btn btn-primary btn-xs" target="_blank" href="{{ url('/aftersale/' . $aftersale->file) }}"><i class="fa fa-folder-open"></i>Curent Contracts</a>
				
					<input type="file" name="file" class="form-control" placeholder="input file">
					{!! $errors->first('file', '<p class="help-block">:message</p>') !!}
				</div>
			
						<div class="form-group">
						<input type="hidden" value="{{csrf_token()}}" name="_token">
							<input type="submit" class="btn btn-primary" value="Simpan">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
