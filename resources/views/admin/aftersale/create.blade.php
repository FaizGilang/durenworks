@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h3>Add After Sale Contracts</h3>
			<div class="panel panel-default">
				<div class="panel-body">
					<form action="{{ url('/admin/aftersale/store') }}" method="post"  enctype="multipart/form-data">
					{{csrf_field()}}
					
				<div class="form-group{{ $errors->has('id_quo') ? ' has-error' : '' }}">
					<label for="select">Select Quotation</label>
						<select name="quo" class="form-control" required="required">
						<option value="" selected>--- Choose Quotation ---</option>
						@foreach($quo as $q)
							<option value="{{ $q->no }}">{{ $q->no }} - {{ $q->proyek }}</option>
						@endforeach
					</select>
					{!! $errors->first('id_quo', '<p class="help-block">:message</p>') !!}
				</div>
				
				<div class="form-group{{ $errors->has('startdate') ? ' has-error' : '' }}">
					<label for="input">Start Date</label>
					<input type="date" name="startdate" class="form-control" placeholder="Input contract name" value="{{ old('startdate') }}">
					{!! $errors->first('startdate', '<p class="help-block">:message</p>') !!}
				</div>
				
				<div class="form-group{{ $errors->has('enddate') ? ' has-error' : '' }}">
					<label for="input">End Date</label>
					<input type="date" name="enddate" class="form-control" placeholder="Input contract name" value="{{ old('enddate') }}">
					{!! $errors->first('enddate', '<p class="help-block">:message</p>') !!}
				</div>
				
				<div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
					<label for="input">File to upload</label>
					<input type="file" name="file" class="form-control" placeholder="input file">
					{!! $errors->first('file', '<p class="help-block">:message</p>') !!}
				</div>
			
						<div class="form-group">
						<input type="hidden" value="{{csrf_token()}}" name="_token">
							<input type="submit" class="btn btn-primary" value="Simpan">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
