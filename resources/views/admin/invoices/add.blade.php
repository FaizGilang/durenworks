@extends('layouts.admin')

@section('content')

<?php
	if (isset($result)) {
	// 	for ($i = 0; $i < count($result); $i++) {
	// 		print_r($result[$i]['phone']);
	// 		echo '<br/>';
	// 	}
		print_r($result);
	}
?>

<div class="form-group">
	<label for="phone" class="control-label">
	Phone Number
	</label>
	<table class="table table-condensed table-border">
		<tbody id="phone"></tbody>
	</table>
</div>

<form method="POST" action="{{ url('/admin/invoice/add') }}">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">

	<input type="hidden" name="vendor_phones" id="vendor_phones" value="[]">
	<input type="hidden" name="desc" id="desc" value="[]">

	<input type="submit" name="save" class="btn btn-success" value="Save">
</form>

<!-- Jquery untuk memudahkan saja dalam manipulasi dom -->
<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('js/mustache.js') }}"></script>

<!--
# script type="x-tmpl-mustache" dengan id tertentu akan dirender pada tempat yg telah ditentukan, dalam hal ini adalah tbody.
# perulangan pada mustache menggunakan tag awalan #variable dan tag penutup /variable dalam hal ini #item dan /item.
# variable yang akan ditampilkan harus ditulis diantara tag pembuka dan penutup sesuai setting.
-->
<script type="x-tmpl-mustache" id="phone_add">
	{% #item %}
		<tr class="text " id="data_{% _id %}">
			<td>{% phone %}</td>
			<td>{% desc %}</td>
			<td>
				<button type="button" id="edit_{% _id %}" class="btn btn-danger btn-xs btn-edit-phone" data-id="{% _id %}">edit</button>
				<button type="button" id="delete_{% _id %}" class="btn btn-danger btn-xs btn-delete-phone" data-id="{% _id %}">hapus</button>
			</td>
		</tr>
		<tr class="form hidden" id="form_{% _id %}">
			<td>
				<input type="text" name="sphone" id="sphone_{% _id %}" value="{% phone %}">
				<input type="text" name="desc" id="desc_{%_id_%}" value="{% desc %}">
			</td>
			<td>
				<button type="button" id="save_{% _id %}" class="btn btn-danger btn-xs btn-save-phone" data-id="{% _id %}">ok</button>
				<button type="button" id="cancel_{% _id %}" class="btn btn-danger btn-xs btn-cancel-phone" data-id="{% _id %}">cancel</button>
			</td>
		</tr>
	{% /item %}

	<tr class="input-data" id="input_data">
		<td>
			<input type="text" name="sphone" id="sphone">
			<input type="text" name="desc" id="desc">
		</td>
		<td>
			<button type="button" id="AddPhone" class="btn btn-success" data-id="">
				Add
			</button>
		</td>
	</tr>
</script>
<script>
// setting tag pembuka dan penutup secara global untuk mustache. Karena secara default tag pembuka dan penutup mustache adalah '{{' dan '}}', hal ini akan mengakibatkan bentrok dengan laravel blade karena tag nya sama.
Mustache.tags = ['{%', '%}'];

$(function(){
	// Mengubah data pada input type hidden ke dalam javascript array.
	var vendor_phones = JSON.parse($('#vendor_phones').val());
	var desc = JSON.parse($('#desc').val());
	// Fungsi render digunakan untuk merender data dan template mustache ke dalam elemen yg sudah ditentukan
	function render()
	{
		// Memanggil fungsi getIndex sebelum ditampilkan
		getIndex();
		// Mengubah list array ke dalam bentuk data json lalu dimasukan ke dalam input type hidden
		$('#vendor_phones').val(JSON.stringify(vendor_phones));
		$('#desc').val(JSON.stringify(desc));
		// Mendefinisikan template
		var tmpl = $('#phone_add').html();
		// parse template ke dalam mustache template
		Mustache.parse(tmpl);
		// Merender dengan template dan data
		// variable 'item' adalah variable yang akan dilooping
		var html = Mustache.render(tmpl, { item : vendor_phones });
		// mengisi html tbody dengan hasil renderan di atas
		$('#phone').html(html);

		// memanggil fungsi bind
		bind();
	}

	// PENTING. fungsi bind digunakan untuk memberikan event listener kepada elemen html setelah render selesai. Jika tidak dilakukan bind maka event pada elemen tidak akan berjalan.
	function bind(){
		// Memberikan event listener pada tombol pada saat tombol diklik
		$('#AddPhone').on('click', add);
		$('.btn-delete-phone').on('click', delete_phone);
		$('.btn-edit-phone').on('click', edit);
		$('.btn-cancel-phone').on('click', canceledit);
		$('.btn-save-phone').on('click', saveedit);
	}

	// Fungsi add untuk menambah item ke dalam list
	function add()
	{
		// Membaca nilai dari inputan
		var phone = $('#sphone').val();

		// Membuat object yg akan dimasukan kedalam array
		var input = {
			'phone' : phone,
		};

		// Memasukan object ke dalam array
		if (phone)
			vendor_phones.push(input);

		// Merender ulang karena data sudah berubah
		render();
	}

	// fungsi edit untuk menampilkan form edit
	function edit()
	{
		// Mengambil id item yang akan dihapus
		var i = parseInt($(this).data('id'), 10);

		var row_data = $("#data_" + i);
		var row_form = $("#form_" + i);
		var input_data = $("#input_data");

		// menyembunyikan input form
		if(!input_data.hasClass('hidden')){
			input_data.addClass('hidden');
		}

		// menyembunyikan baris data
		if(!row_data.hasClass('hidden')){
			row_data.addClass('hidden');
		}

		// menampilkan baris form
		if(row_form.hasClass('hidden')){
			row_form.removeClass('hidden');
		}
	}

	// fungsi edit untuk menyembunyikan form edit
	function canceledit()
	{
		// Cara hide form edit 1 >>>
		// // Mengambil id item yang akan dihapus
		// var i = parseInt($(this).data('id'), 10);

		// var row_data = $("#data_" + i);
		// var row_form = $("#form_" + i);
		// var input_data = $("#input_data");

		// // menampilkan input form
		// if(input_data.hasClass('hidden')){
		// 	input_data.removeClass('hidden');
		// }

		// // menampilkan baris data
		// if(row_data.hasClass('hidden')){
		// 	row_data.removeClass('hidden');
		// }

		// // menyembunyikan baris form
		// if(!row_form.hasClass('hidden')){
		// 	row_form.addClass('hidden');
		// }

		// <<< end of cara hide edit 1

		// Cara hide form edit 2 >>>
		// Merender ulang
		render();

		// <<< end of cara hide edit 2
	}

	function saveedit()
	{
		// Mengambil id item yang akan dihapus
		var i = parseInt($(this).data('id'), 10);

		// Membaca nilai dari inputan
		var phone = $('#sphone_' + i).val();

		// Menyimpan data pada list
		vendor_phones[i].phone = phone;

		// Merender kembali karena data sudah berubah
		render();
	}

	// Fungsi delete_phone digunakan untuk menghapus elemen
	function delete_phone(){
		// Mengambil id item yang akan dihapus
		var i = parseInt($(this).data('id'), 10);

		// menghapus list dari elemen array
		vendor_phones.splice(i, 1);

		// Merender kembali karena data sudah berubah
		render();
	}

	// Fungsi getIndex digunakan untuk membuat penomoran dan id unik sebelum dirender
	function getIndex()
	{
		for (idx in vendor_phones) {
			// setting _id digunakan untuk memudahkan dalam mengedit dan menghapus list, seperti id pada tabel dalam database.
			vendor_phones[idx]['_id'] = idx;
			// setting no digunakan untuk penomoran
			vendor_phones[idx]['no'] = parseInt(idx) + 1;
		}
	}

	// Memanggil fungsi render pada saat halaman pertama kali dimuat
	render();
});
</script>
@endsection
