@component('mail::message')

# {{ $content['title'] }}


{{ $content['body'] }}


@component('mail::table')

to extend it you can click :

@endcomponent


@component('mail::button', ['url' => ''])

{{ $content['button'] }}

@endcomponent


Thanks,

{{ config('app.name') }}

@endcomponent