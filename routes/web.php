<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::get('sendmail', 'SendMailController@sendMail');
Route::get('/admin/send', 'SendMailController@send');

Route::get('/home', 'HomeController@index');

Route::group(['middleware'=>['auth']], function (){
    Route::get('/admin', 'HomeController@admin');
	Route::get('/', 'HomeController@admin');


    /*projects controller
    consist of
    create
    retrieve
    update
    Delete
    */
	//index
    Route::get('/admin/project', 'ProjectController@index');
	//add
    Route::get('/admin/project/create', 'ProjectController@create');
    Route::post('/admin/project/store', 'ProjectController@store');
	//edit
	Route::get('/admin/project/{id}/edit/', 'ProjectController@edit');
    Route::post('/admin/project/update/', 'ProjectController@update');

    Route::get('/admin/project/contact/{id}', 'ProjectController@contact');
    Route::get('/admin/project/getindex/{get}', 'ProjectController@getindex');
	//delete
	Route::get('/admin/project/destroy/{id}', 'ProjectController@destroy');


    /*quotation controller
    consist of
    create
    Retrieve
    update
    Delete
    */
    //create form for quotation
    Route::get('/admin/quotation/add', 'QuotationsController@add1');
	Route::get('/admin/quotation/add/{id}', 'QuotationsController@add2');
    //store quotation data
    Route::post('/admin/quotation/store', 'QuotationsController@store');
    //retrieve quotations
    Route::get('/admin/quotation', 'QuotationsController@index');
    Route::get('/admin/quotation/draft', 'QuotationsController@draft');
    //retrieve specific quotation
    Route::get('/admin/quotation/{id}', 'QuotationsController@show');
    //edit form for specific quotation
    Route::get('/admin/quotation/{id}/edit', 'QuotationsController@edit');
    //update quotation
    Route::post('/admin/quotation/save', 'QuotationsController@update');
    //reject quotation
    Route::get('/admin/quotation/delete/{id}', 'QuotationsController@destroy');
	//Accept quotation
    Route::get('/admin/quotation/accept/{id}', 'QuotationsController@accept');

    /*Contract controller
    consist of
    retrieve
    create
    update
    Delete
    */
    //retrieve contracts
    Route::get('/admin/contract', 'ContractsController@index');
    //display add contract form
    Route::get('/admin/contract/add', 'ContractsController@create');
    //store contract data
    Route::post('/admin/contract/store', 'ContractsController@store');
    //delete contract data
    Route::get('/admin/contract/delete/{id}', 'ContractsController@destroy');
    //retrieve specific contract
    Route::get('/admin/contract/{id}', 'ContractsController@show');
    //edit form for specific contract
    Route::get('/admin/contract/{id}/edit', 'ContractsController@edit');
    //update contract
    Route::post('/admin/contract/save', 'ContractsController@update');

    /*Invoice Controller
    consist of
    retrieve
    create
    update
    Delete
    */
    //retrieve Invoice
    Route::get('/admin/invoice', 'InvoicesController@index');
    //display invoices add form
    Route::get('/admin/invoice/add', 'InvoicesController@create');
    //store invoices data
    Route::post('/admin/invoice/add', 'InvoicesController@store');
    //display invoices based on quotation number
    Route::get('/admin/invoice/{id}', 'InvoicesController@show');
    // Route::post('/admin/invoice', 'InvoicesController@index');
	
	//AfterSale Controller
	//retrieve
    Route::get('/admin/aftersale', 'AfterSaleController@index');
	//display invoices add form
    Route::get('/admin/aftersale/add', 'AfterSaleController@create');
	Route::post('/admin/aftersale/store', 'AfterSaleController@store');
	//edit
	Route::get('/admin/aftersale/edit/{id}', 'AfterSaleController@edit');
    Route::post('/admin/aftersale/update/', 'AfterSaleController@update');
});
