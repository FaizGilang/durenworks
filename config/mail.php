<?php

return [

'driver' => env('MAIL_DRIVER', 'smtp'),
'host' =>env('MAIL_HOST', 'smtp.gmail.com'),
'port' =>env('MAIL_PORT', 587),
'from' => ['address' =>'kishinohikari@gmail.com', 'name' => 'Email_Subject'],
'encryption' => env('MAIL_ENCRYPTION', 'tls'),
'username' =>env('MAIL_USERNAME','kishinohikari@gmail.com'),
'password' =>env('MAIL_PASSWORD','dgoldenz'),
'sendmail' =>'/usr/sbin/sendmail -bs',

    /*
    |--------------------------------------------------------------------------
    | Markdown Mail Settings
    |--------------------------------------------------------------------------
    |
    | If you are using Markdown based email rendering, you may configure your
    | theme and component paths here, allowing you to customize the design
    | of the emails. Or, you may simply stick with the Laravel defaults!
    |
    */

    'markdown' => [
        'theme' => 'default',

        'paths' => [
            resource_path('views/vendor/mail'),
        ],
    ],

];
