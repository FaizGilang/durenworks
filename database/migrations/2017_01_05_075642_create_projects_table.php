<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company');
            $table->string('konsumen');
            $table->string('email')->unique();
            $table->string('hp')->unique();
             $table->string('alamat');
            $table->string('status');
            $table->timestamps();
        });

        DB::table('projects')->insert([
            [
                'company' => 'Durenworks',
                'konsumen' => 'Norfa Bagas',
                'email' => 'bgsnorfa@gmail.com',
                'hp' => '082138376922',
                'alamat' => 'jalan raya',
                'status' => 'Prospecting'
            ],
            [
                'company' => 'Durenworks',
                'konsumen' => 'Nama',
                'email' => 'mail@mail.com',
                'hp' => '081222',
                'alamat' => 'jalan tidak raya',
                'status' => 'Prospecting'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
