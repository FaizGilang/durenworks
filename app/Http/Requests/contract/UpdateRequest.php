<?php

namespace App\Http\Requests\project;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules(Request $request)
    {
        return [
            'nama'=> 'Required',
			'email'=> 'Required|unique:projects,email|email',Rule::unique('projects')->ignore($request->id),
            'phone'=> 'Required|unique:projects,hp',Rule::unique('projects')->ignore($request->id),
        ];
    }

    public function messages()
    {
        return [
            'nama.required' => 'Nama Tidak Boleh Kosong.',
			
            'email.required' => 'Email Tidak Boleh Kosong.',
            'email.unique' => 'Email Sudah Terpakai.',
            'email.email' => 'Format Email Tidak Benar',
			
            'phone.required' => 'Nomor Handphone Tidak Boleh Kosong.',
            'phone.unique' => 'Nomor Handphone Sudah dipakai.'
        ];
    }
}
