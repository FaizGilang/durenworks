<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quotations;
use App\Project;
use Redirect;
use App\Http\Requests\quotation\StoreRequest;
use App\Http\Requests\quotation\UpdateRequest;
use DB;

class QuotationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('s')){
            $quotations = DB::table('quotations')
				->join('projects','quotations.konsumen','=','projects.id')
				->where('quotations.proyek', 'LIKE', '%'.$request->s.'%')
				->select('quotations.*', 'projects.company')->paginate(10)->appends(['s' => $request->s]);
			}
		else{
            $quotations = DB::table('quotations')
				 ->join('projects','quotations.konsumen','=','projects.id')
				 ->select('quotations.*', 'projects.company')->paginate(10);

        }
        return view('admin.quotations.index')->with('quotations', $quotations);
    }
	 public function draft(Request $request)
    {
        if($request->has('s')){
            $quotations = Quotations::where('proyek', 'LIKE', '%'.$request->s.'%')->paginate(10)->appends(['s' => $request->s]);
        } else {
            $quotations = Quotations::where('no', '=', '')->paginate(10);
        }
        return view('admin.quotations.index')->with('title', 'Quotations')->withQuotations($quotations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add2($id)
    {
        return view('admin.quotations.add')->with('title', 'Add new Quotation')->with('id', $id);
    }

	 public function add1()
    {
        return view('admin.quotations.add')->with('title', 'Add new Quotation');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //save input data to variable
        // $konsumen = $request->input('konsumen');
        $proyek = $request->input('proyek');
        // $alamat = $request->input('alamat');
        $term = $request->input('term');
        $waktu = $request->input('waktu');
        $spec = $request->input('spesifikasi');
        $harga = $request->input('harga');

	if($request->input('save')=='Create')
	{
	 $this->validate($request, [
        'term' => 'required',
    ]);
        if (!$request->id) {
            $projects = new Project();
    		$projects->konsumen=$request->konsumen;
    		$projects->email=$request->email;
    		$projects->hp=$request->phone;
    		$projects->deskripsi=$request->deskripsi;
    		$projects->alamat=$request->alamat;
    		$projects->status="Discussed";
            $projects->save();
            $id = $projects->id;
        }

        $quo = new Quotations();
        //insert quotation
        if (!$request->id) {
            $quo->konsumen = $id;
        } else {
            $quo->konsumen = $request->id;
        }

        $quo->proyek = $proyek;
        //$quo->alamat = $alamat;
        $quo->term =  $term;
        $quo->waktu = $waktu;
        $quo->spesifikasi = $spec;
        $quo->admin = $request->user()->name;
        $quo->harga = $harga;
        $quo->save();

        //update no field with last inserted id
        $quot = Quotations::find($quo->id);
        $quot->no = 'QUO' . date('d') . date('m') . $quo->id;
        $quot->save();
		return redirect('/admin/quotation')->with('message', 'New quotation added !');
	}
		if($request->input('save')=='Save'){

            if (!$request->id) {
                $projects = new Project();
        		$projects->konsumen=$request->konsumen;
        		$projects->email=$request->email;
        		$projects->hp=$request->phone;
        		$projects->deskripsi=$request->deskripsi;
        		$projects->alamat=$request->alamat;
        		$projects->status="Discussed";
                $projects->save();
                $id = $projects->id;
            }

		 $quo = new Quotations();
        //insert quotation
        // $quo->konsumen = $konsumen;
        if (!$request->id) {
            $quo->konsumen = $id;
        } else {
            $quo->konsumen = $request->id;
        }
        $quo->proyek = $proyek;
        // $quo->alamat = $alamat;
        $quo->term =  $term;
        $quo->waktu = $waktu;
        $quo->spesifikasi = $spec;
        $quo->admin = $request->user()->name;
        $quo->harga = $harga;
        $quo->save();

        //update no field with last inserted id
		$quot = Quotations::find($quo->id);
        $quot->no = '';
        $quot->save();
		return redirect('/admin/quotation/draft')->with('message', 'New quotation added !');
	}


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $quotation = Quotations::get()->where('id', $id)->first();
        if(!count($quotation)){
            return redirect('/admin/quotation');
        }
        return view('admin.quotations.show')->with('title', $quotation->proyek)->with('quotation', $quotation);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quotation = Quotations::get()->where('id', $id)->first();
        return view('admin.quotations.edit')->with('title', 'Quotation edit : ' . $quotation->proyek)->with('quotation', $quotation);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //get specific quotation
        $quo_id = $request->input('quo_id');
        $quo = Quotations::find($quo_id);

        //save input data to variable
        $status = $request->input('status');
        $konsumen = $request->input('konsumen');
        $proyek = $request->input('proyek');
        $alamat = $request->input('alamat');
        $term = $request->input('term');
        $waktu = $request->input('waktu');
        $spec = $request->input('spesifikasi');
		$harga = $request->input('harga');
		if($quo->no==''){
			$no = 'QUO' . date('d') . date('m') . $quo->id;
		}
		if($quo->no!=''){
			$no = $quo->no;
		}
        //checking if data is null
        if(empty($status)){
            $status = $quo->status;
        }
        if(empty($konsumen)){
            $konsumen = $quo->konsumen;
        }
        if(empty($proyek)){
            $proyek = $quo->proyek;
        }
        if(empty($alamat)){
            $alamat = $quo->alamat;
        }
        if(empty($term)){
            $term = $quo->term;
        }
        if(empty($waktu)){
            $waktu = $quo->waktu;
        }
        if(empty($spec)){
            $spec = $quo->spesifikasi;
        }

	if($request->input('save')=='Create')
	{
        //assign / update quotation
        $quo->status = $status;
        $quo->konsumen = $konsumen;
        $quo->alamat = $alamat;
        $quo->proyek = $proyek;
        $quo->term =  $term;
        $quo->waktu = $waktu;
        $quo->spesifikasi = $spec;
        $quo->no = $no;
        $quo->save();
        return redirect('/admin/quotation')->with('message', 'Quotation updated');
    }
	if($request->input('save')=='Save')
	{
		$quo->konsumen = $konsumen;
        $quo->proyek = $proyek;
        $quo->alamat = $alamat;
        $quo->term =  $term;
        $quo->waktu = $waktu;
        $quo->spesifikasi = $spec;
        $quo->admin = $request->user()->name;
        $quo->harga = $harga;
        $quo->save();

        //update no field with last inserted id
		$quot = Quotations::find($quo->id);
        $quot->no = '';
        $quot->save();
		return redirect('/admin/quotation/draft')->with('message', 'New quotation added !');
	}
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quo = Quotations::find($id);
		$quo->status="Rejected";
        $msg = $quo->no;
        $quo->save();
        return redirect('/admin/quotation')->with('message', 'Quotation ' .$msg. ' Deleted');
    }
	    public function accept($id)
    {
        $quo = Quotations::find($id);
		$quo->status="Aproved";
        $msg = $quo->no;
        $quo->save();
        return redirect('/admin/quotation')->with('message', 'Quotation ' .$msg. ' Approved');
    }
}
