<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Contracts;
use App\Quotations;
use Redirect;
use File;
use Storage;
use App\Http\Requests\contract\StoreRequest;
use App\Http\Requests\contract\UpdateRequest;

class ContractsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('s')){
            $contracts = Contracts::where('nama', 'LIKE', '%'.$request->s.'%')->paginate(10)->appends(['s' => $request->s]);
        } else {
            $contracts = Contracts::paginate(10);
        }
        return view('admin.contracts.index')->with('title', 'Contracts')->with('contracts', $contracts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $quo = Quotations::get()->where('status', '=', 'aproved');
        return view('admin.contracts.add')->with('title', 'Create new Contract')->with('quo', $quo);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $save = new Contracts();
        $save->id_quo = $request->get('quo');
        $save->nama = $request->get('nama');
        $save->deskripsi = $request->get('deskripsi');

        $save->save();

        //update current quotation status
        $quo = Quotations::where('no', '=', $request->get('quo'))->first();
        $quo->status = 'accepted';
        $quo->oncontract = 'yes';
        $quo->save();

        //upload image
        $saving = Contracts::find($save->id);
        //get same file name
        $count = Contracts::where('id_quo', '=', $request->get('quo'))->count();

        //file upload
        if($request->file('file')){
            //get file extension
            $ext = Input::file('file')->getClientOriginalExtension();

            $fileName = 'Contracts-' . $request->get('quo') . '.' . $ext;
            $request->file('file')->move('contracts/',$fileName);
        }
        $saving->file = $fileName;
        $saving->save();
        return redirect('/admin/contract')->with('message', 'New contract added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contract = Contracts::find($id)->first();
        return view('admin.contracts.show')->with('title', $contract->nama)->with('contract', $contract);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contract = Contracts::find($id)->first();
        return view('admin.contracts.edit')->with('title', 'Edit Contract : ' . $contract->nama )->with('contract', $contract);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {
        $id_contract = $request->get('id_contract');
        $contract = Contracts::find($id_contract);

        //checking whether new file is uploaded
        if($request->file('file')){
            File::delete('contracts/' . $contract->file);
            echo 'ada file yang akan di upload<br/><br/>';
            //upload new file
            $ext = Input::file('file')->getClientOriginalExtension();

            $fileName = 'Contracts-' . $request->get('quo') . '.' . $ext;
            $request->file('file')->move('contracts/',$fileName);
        }

        //save input data to variable
        $nama = $request->get('nama');
        $deskripsi = $request->get('deskripsi');

        if(empty($nama)){
            $nama = $contract->nama;
        }

        if(empty($deskripsi)){
            $deskripsi = $contract->deskripsi;
        }

        $contract->nama = $nama;
        $contract->deskripsi = $deskripsi;
        $contract->save();

        return redirect('/admin/contract')->with('message', 'Contract updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $find = Contracts::find($id);

        //checking if file in contract is available
        if($find->file){
            File::delete('contracts/' . $find->file);
        }

        //update related quotation status to waiting
        $quo = $find->id_quo;
        $cari_quo = Quotations::where('no', $quo)->first();
        $cari_quo->status = 'waiting';
        $cari_quo->save();

        $find->delete();
        return redirect('/admin/contract');
    }
}
