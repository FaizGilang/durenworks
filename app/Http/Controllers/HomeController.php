<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quotations;
use App\Invoices;
use App\Project;
use App\AfterSale;
use App\Contracts;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
	
	public function admin()
	{
		$data['project'] = Project::get()->count();
		$data['quotation'] = Quotations::get()->where('no', '!=', '')->count();
		$data['contract'] = Contracts::get()->count();
		$data['aftersale'] = AfterSale::get()->count();
		return view('admin.index')->with('data', $data);
	}
}
