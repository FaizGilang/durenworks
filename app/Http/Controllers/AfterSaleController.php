<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\AfterSale;
use App\Quotations;
use App\Http\Requests;
use DB;
use File;
use Storage;


class AfterSaleController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aftersales = AfterSale::all();
        return view('admin.aftersale.index', compact('aftersales'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $quo = Quotations::get()->where('status', '=', 'accepted')->where('onaftersale', '=', 'no');
        return view('admin.aftersale.create')->with('title', 'Create After Sale Contracts')->with('quo', $quo);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = new AfterSale();
        $save->id_quo = $request->get('quo');
        $save->startdate = $request->get('startdate');
        $save->enddate = $request->get('enddate');
		 if($request->file('file')){
        //file upload
           //get file extension
            $ext = Input::file('file')->getClientOriginalExtension();

            $fileName = 'AfterSale-' . $request->get('quo') . '.' . $ext;
            $request->file('file')->move('aftersale/',$fileName);
		 }
        $save->file = $fileName;
		
        $save->save();
		
		//update current quotation status
        $quo = Quotations::where('no', '=', $request->get('quo'))->first();
        $quo->onaftersale = 'yes';
        $quo->save();
		
        return redirect('/admin/aftersale')->with('message', 'New After Sale Contract added!');
    }
	public function edit($id)
    {
        $aftersale = AfterSale::find($id);
        return view('admin.aftersale.edit', compact('aftersale'));
    }
	public function update(request $request)
    {
		$id = $request->id;
        $save = Project::find($id);
		$save->enddate=$request->enddate;
		if($request->file('file')){
        //file upload
           //get file extension
            $ext = Input::file('file')->getClientOriginalExtension();

            $fileName = 'AfterSale-' . $request->get('quo') . '.' . $ext;
            $request->file('file')->move('aftersale/',$fileName);
		 }
        $save->save();
        return  redirect('/admin/aftersale')->with('alert-success', 'Data Berhasil Diubah.');
    }
}