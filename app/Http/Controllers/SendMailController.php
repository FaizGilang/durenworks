<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Mail\OrderShipped;

use Mail;


class SendMailController extends Controller

{

    /**

     * Show the application sendMail.

     *

     * @return \Illuminate\Http\Response

     */

    public function sendMail()

    {

    	$content = [

    		'title'=> 'Durenworks After Sale Notification', 

    		'body'=> 'we humbly inform you that your project with quo-11121 will end soon',

    		'button' => 'Click Here'

    		];


    	$receiverAddress = 'bgsnorfa@gmail.com';


    	Mail::to($receiverAddress)->send(new OrderShipped($content));


    	dd('mail send successfully');

    }

}