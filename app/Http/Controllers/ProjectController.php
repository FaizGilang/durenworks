<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Http\Requests;
use App\Http\Requests\Project\StoreRequest;
use App\Http\Requests\Project\UpdateRequest;
use DB;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(request $request)
    {
		if($request->has('s')){
            $projects = Project::where('company', 'LIKE', '%'.$request->s.'%')->paginate(10)->appends(['s' => $request->s]);
        } else {
			$projects = Project::all();
		}
        return view('admin.projects.index', compact('projects'));
		
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
		$projects = new Project();
		$projects->company=$request->company;
		$projects->konsumen=$request->nama;
		$projects->email=$request->email;
		$projects->hp=$request->phone;
		$projects->alamat=$request->alamat;
		$projects->status="Prospecting";
        $projects->save();
        return redirect('/admin/project')->with('alert-success', 'Add Data Success.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$projects = Project::findOrFail($id)->first();
        $projects = Project::find($id);
        return view('admin.projects.edit', compact('projects'));
		//return view('edit')->with('projects',$projects);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {
		$id = $request->id;
        $projects = Project::find($id);
		$projects->company=$request->company;
		$projects->konsumen=$request->nama;
		$projects->email=$request->email;
		$projects->hp=$request->phone;
		$projects->alamat=$request->alamat;
        $projects->save();
        return  redirect('/admin/project')->with('alert-success', 'Successfully Edited');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $projects = Project::findOrFail($id);
        $projects->delete();
        return  redirect('/admin/project')->with('alert-success', 'Data Berhasil Dihapus.');
    }

	public function contact($id)
	{
		$projects = Project::findOrFail($id);
		$projects->status="Contacted";
        $projects->save();
        return redirect('/admin/project')->with('alert-success', 'Contacted.');
	}

	public function getIndex($get){
	  $projects = DB::table('projects');
	  if($get=='prospecting'){
		$projects->where('projects.status','Prospecting');
	  }
	  if($get=='contacted'){
		$projects->where('projects.status','Contacted');
	  }
	  if($get=='all'){
		$projects;
	  }
	  $projects = $projects->paginate(10);
	  return view('admin.projects.index',['projects' => $projects]);
	}

}
