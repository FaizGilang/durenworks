<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\AfterSale;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	view()->composer('layouts.admin', function($view)
    {
		$variable2 = AfterSale::get()->count();
        $view->with('variable2', $variable2);
    });
       
       
	    Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
